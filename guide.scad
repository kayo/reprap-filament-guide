translate([-18, -4, 2])
rotate([0, -90, 0])
import("hold.stl");

translate([0, 0, 2])
rotate([0, 90, 0])
import("yoke.stl");

translate([-16, -5, 0]){
    for(i=[0:1]){
        translate([0, i*13, 0])
        import("roll.stl");
    }
}
